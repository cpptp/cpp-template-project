#!/usr/bin/env bash

# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

#------------------------------------------------------------------------------#
# Official Red Hat Enterprise Linux RPM software packages                      #
#------------------------------------------------------------------------------#

# How to install gcc/g++ 9 on CentOS 8 docker (centos:latest)
# See https://stackoverflow.com/questions/61590926

declare -a INSTALL_PKGS=(
  "clang"                         # 11.0.0
  "cmake"                         # 3.18.2
  "gcc-toolset-10-gcc"            # 10.2.1
  "gcc-toolset-10-gcc-c++"        # 10.2.1
  "gcc-toolset-10-gcc-gfortran"   # 10.2.1
  "gcc-toolset-10-gdb"            # 8.3
  "gcc-toolset-10-gdb"            # 8.3
  "gcc-toolset-10-libasan-devel"  # 10.2.1 (The Address Sanitizer)
  "gcc-toolset-10-liblsan-devel"  # 10.2.1 (The Leak Sanitizer)
  "gcc-toolset-10-libtsan-devel"  # 10.2.1 (The Thread Sanitizer)
  "gcc-toolset-10-libubsan-devel" # 10.2.1 (The Undefined Behavior Sanitizer)
  "git"                           # whatever
  "make"                          # 4.2.1
  "python3-pip"                   # python 3.6 + pip3 9.0.3
)

# Fix doxygen

# See https://pagure.io/epel/issue/89
# See https://serverfault.com/questions/997896

yum install -y dnf-plugins-core
yum config-manager --set-enabled powertools

## Install packages

yum install -y --setopt=tsflags=nodocs ${INSTALL_PKGS[@]}
rpm -V ${INSTALL_PKGS[@]}
yum -y clean all --enablerepo='*'

#------------------------------------------------------------------------------#
# Python                                                                       #
#------------------------------------------------------------------------------#

pip3 install -r 'requirements.txt'

#------------------------------------------------------------------------------#
# Setup environment                                                            #
#------------------------------------------------------------------------------#

#
# Clang
#

# See https://gatowololo.github.io/blog/clangmissingheaders/
# See https://stackoverflow.com/questions/26333823

# rm -rf /usr/include/c++
# ln -s /opt/rh/gcc-toolset-10/root/usr/include/c++/10/ /usr/include/c++

# ln -s /opt/rh/gcc-toolset-10/root/lib/gcc/x86_64-redhat-linux/10/ /usr/lib/gcc/x86_64-redhat-linux/10

#
# SCL (Software Collections)
#

# Make SCL avaible globaly in user environment.
# https://stackoverflow.com/questions/61590926
echo "source /opt/rh/gcc-toolset-10/enable" >> /etc/bashrc
