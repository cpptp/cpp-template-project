# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# We are trying to utilize all sanitazers, so...
#   - build:centos7:clang
#       - [NO LTO]
#   - build:centos7:gcc
#       - nothing
#   - build:centos8:clang
#       - [NO LTO]
#       - memory
#   - build:centos8:gcc
#       - address
#       - leak
#       - undefined behavior
#   - build:ubuntu2004:clang
#       - thread
#   - build:ubuntu2004:gcc
#       - gcov

.build:
  stage: build
  extends: .only_review_all_cpp
  interruptible: false
  before_script:
    # nproc on GNU/Linux, sysctl on macOS
    - NPROC=$(nproc || sysctl -n hw.physicalcpu) 2>/dev/null
  script:
    - mkdir build
    - cmake -S. -Bbuild ${CMAKE_ARGS} 2>&1 | tee build/ConfigurationLog.txt
    - cmake --build build -j${NPROC} 2>&1 | tee build/CompilationLog.txt
  artifacts:
    expire_in: 2 hrs
    when: always
    paths:
      # Different logs are needed if something happens.
      - build/CMakeFiles/CMakeError.log
      - build/CMakeFiles/CMakeOutput.log
      - build/ConfigurationLog.txt
      - build/CompilationLog.txt

      # The main binaries.
      - build/bin/

      # The cache is needed for the installation test.
      - build/CMakeCache.txt

      # CTest files are needed for the installation test.
      - build/CTestTestfile.cmake
      - build/**/CTestTestfile.cmake

.test:
  stage: test
  extends: .only_review_all_cpp
  when: on_success
  variables:
    BOOST_TEST_LOGGER: "JUNIT,all,test_results.xml:HRF,warning"
    CTEST_OUTPUT_ON_FAILURE: 1
    GIT_CHECKOUT: "false"
    GIT_STRATEGY: none
  before_script:
    # nproc on GNU/Linux, sysctl on macOS
    - NPROC=$(nproc || sysctl -n hw.physicalcpu) 2>/dev/null
  script:
    - cd build
    - ctest -j${NPROC}
  artifacts:
    reports:
      junit:
        - build/project/*/tests/*/test_results.xml

# If build job failed, upload compilation logs in an easily downloadable form.
.report-build:
  extends:
    - .only_review_all_cpp
    - .report
  script:
    - cp /build/CMakeFiles/CMakeError.log .
    - cp /build/CMakeFiles/CMakeOutput.log .
  artifacts:
    expire_in: 1 day
    expose_as: 'Build log'
    name: "BuildLog-$CI_COMMIT_REF_SLUG"
    paths:
      - CMakeError.log
      - CMakeOutput.log
      - ConfigurationLog.txt
      - CompilationLog.txt
