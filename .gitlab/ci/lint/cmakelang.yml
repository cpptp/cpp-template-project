# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.check-cmake:
  extends:
    - .check
    - .only_review_cmake

.report-cmake:
  extends:
    - .only_review_cmake
    - .report

# Format

check:cmake:format:
  extends: .check-cmake
  allow_failure: true
  script:
    # Install packages
    - pip install --trusted-host=pypi.org --user cmakelang
    # Perform ckeck
    - |-
        # Extract files
        CHANGED_FILES=$(echo "${CI_MERGE_REQUEST_CHANGED_FILES}" | grep --color=never -E "CMakeLists.txt|.*\.cmake$")
        if [[ -n "${CHANGED_FILES}" ]]; then
          if [[ "${CI_LINTER_TRACE}" == "true" ]]; then
            echo -e "\033[0;33mDetected changes in ${CHANGED_FILES}\033[0m"
          fi
          # Perform ckeck
          cmake-format --disabled-codes --in-place ${CHANGED_FILES}
          # Fail if there are some changes
          git diff > cmake_format.patch
          bat --color=always -pp cmake_format.patch
          git diff-index --quiet HEAD --
        fi
  artifacts:
    when: on_failure
    expire_in: 1 day
    expose_as: 'CmakeFormat Report'
    name: "CmakeFormat-$CI_COMMIT_REF_SLUG"
    paths:
      - cmake_format.patch

# Lint

check:cmake:lint:
  extends: .check-cmake
  script:
    # Install packages
    - pip install --trusted-host=pypi.org --user cmakelang
    # Perform ckeck
    - |-
        # Extract files
        CHANGED_FILES=$(echo "${CI_MERGE_REQUEST_CHANGED_FILES}" | grep --color=never -E "CMakeLists.txt|.*\.cmake$")
        if [[ -n "${CHANGED_FILES}" ]]; then
          if [[ "${CI_LINTER_TRACE}" == "true" ]]; then
            echo -e "\033[0;33mDetected changes in ${CHANGED_FILES}\033[0m"
          fi
          # Perform ckeck
          cmake-lint ${CHANGED_FILES} | tee cmake_lint.patch
        fi
  artifacts:
    when: on_failure
    expire_in: 1 day
    expose_as: 'CmakeLint Report'
    name: "CmakeLint-$CI_COMMIT_REF_SLUG"
    paths:
      - cmake_lint.patch
